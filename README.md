# CoiDroid-Chat

> ### 현재 전체기능 미구현상태 [개발 중..]
> + Link: [coidroid](http://coidroid.com)
> + 서버가 가동중일때 접속 가능합니다.
> ### 개발자 정보
> + 이름: 이근혁 (Leegeunhyeok)
> + 이메일: lghlove0509@naver.com

> # 설명
> 채팅 및 사용자 메시징 알림 서비스<br><br>
> Vue.js + Node.js + MariaDB
> # 주요기능
> + 로그인 및 회원가입
> + 친구 추가 기능
> + 온라인, 오프라인 여부 확인
> + 전체 채팅
> + 친구에게 알림 전송 (편지기능)
> + 알림 확인 기능
> + SPA (Single Page Application)

## 초기설정 

``` bash
# 의존성 모듈 설치 
npm install

# 개발 서버 실행 (localhost:8080) + Hot reload
npm run dev

# 빌드
npm run build
```

## 본 서버실행 

```	bash
# 디렉토리 구조 
server 
├── css
│   └── # .css 
├── fonts
│   └── # font 파일
├── js
│   └── # .js
├── node_modules
│   └── # npm install 후 모듈 저장공간
├── public
│   └── # .html 
├── app.js # Server code
└── package.json 

# 서버 의존성 모듈 설치
npm install

# 서버 실행 
npm run start # 개발서버와 실행 X, 빌드 후 기능작동 확인 및 구현은 본 서버로 진행
```

## 데이터베이스 정보
+ MariaDB 또는 MySQL로 사용
```	sql
# DB : coidroid_chat

# 테이블 목록 
# ├── user
# ├── notice
# .. (Updating)

CREATE TABLE user (
	...
)

```
문의사항과 같은 내용은 위의 개발자 이메일로 보내주세요
