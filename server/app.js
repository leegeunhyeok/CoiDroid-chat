/* CoiDroid-chat */
/*  Leegeunhyeok */

var express = require('express'),
    http = require('http')

var fs = require('fs'),
    path = require('path')

var cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    session = require('express-session')({
      secret: 's3ssi0nK3y',
      resave: false,
      saveUninitialized: true
    })
var moment = require('moment')

var mysql = require('mysql2')

// DB Connection Information 
var connection = mysql.createConnection({
  host: 'localhost',
  port: 3306,
  user: 'root',
  password: '1234',
  database: 'coidroid_chat'
})

var app = express()
// Middleware Setting 
app.use('/js', express.static('js'))
app.use('/css', express.static('css'))
app.use('/fonts', express.static('fonts'))
app.use('/icon', express.static('icon'))
app.use('/dist', express.static('../dist'))
app.use(cookieParser())
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())
app.use(session)

// Read file
function readHtml(path, res) {
  fs.readFile(path, (err, data) => {
    res.writeHead(200, {'Content-Type':'text/html'})
    res.end(data)
  })
}

// Get express router object 
var router = express.Router()

// Root path 
router.route('/').get((req, res) => {
  readHtml('public/index.html', res)
})

// Login 
router.route('/process/login').post((req, res) => {
  var email = req.body.email
  var password = req.body.password  
  var query = `SELECT * FROM user WHERE _id='${email}' AND password='${password}'`

  console.log(query)

  connection.query(query, (err, rows, fields) => {
    if(err) {
      console.log(err)
      res.json({user: null})
    } else {
      if(rows.length > 0){
        var user = {id: email, name: rows[0].name}
        req.session.user = user
        res.json({user: user})
      } else {
        res.json({user: null})
      }
    }
  })
})

// Email check
router.route('/process/emailCheck').post((req, res) => {
  var email = req.body.email
  var query = `SELECT * FROM user WHERE _id='${email}'`

  console.log(query)

  connection.query(query, (err, rows, fields) => {
    if(err) {
      console.log(err)
      res.json({available: false})
    } else {
      if(rows.length === 0) {
        res.json({available: true})
      } else {
        res.json({available: false})
      }
    }
  })
})

// Check session
router.route('/process/sessionCheck').post((req, res) => {           
  res.json({user: req.session.user})
})

// User register 
router.route('/process/register').post((req, res) => {
  var name = req.body.name
  var email = req.body.email
  var password = req.body.password
  var time = moment().format('YYYY-MM-DD')
  var query = `INSERT INTO user VALUES ('${email}', '${password}', '${name}', '${time}')`

  console.log(query)

  connection.query(query, (err, rows, fields) => {
    if(err){
      console.log(err)
      res.json({success: false})
    } else {
      if(rows.affectedRows > 0) {
        res.json({success: true})
      } else {
        res.json({success: false})
      }
    }
  })
})

// Get user's friends data 
router.route('/process/getFriends').post((req, res) => {
  var user = req.session.user

  var friend1 = () => {
    return new Promise((resolve, reject) => {
      var query1 = `SELECT _id AS id, name FROM user WHERE _id IN (SELECT user2_id FROM friend WHERE user1_id = '${user.id}' AND accept = true);`
      connection.query(query1, (err, rows) => {
        if(err) {
          console.log(err)
          resolve(null)
        } else {
          resolve(rows)
        }
      })
    })
  }
  
  var friend2 = () => {
    return new Promise((resolve, reject) => {
      var query2 = `SELECT _id AS id, name FROM user WHERE _id IN (SELECT user1_id FROM friend WHERE user2_id = '${user.id}' AND accept = true);`
      connection.query(query2, (err, rows) => {
        if(err) {
          console.log(err)
          resolve(null)
        } else {
          resolve(rows)
        }
      })
    })
  }
  
  var data = []
  friend1().then(result1 => {
    data = result1
    return friend2()
  }).then(result2 => {
    data = data.concat(result2)
    res.json({err: false, friends: data})
  }).catch(err => {
    res.json({err: true, friends: null})
  })
})

// Get user's notice data 
router.route('/process/getNotice').post((req, res) => {
  var user = req.session.user
  var data = {}
  
  var message = () => {
    return new Promise((resolve, reject) => {
      var query = `SELECT notice_id, from_id, name, to_id, content, notice.date AS 'date', rflag AS 'read' FROM notice, user WHERE notice.from_id = user._id AND to_id = '${user.id}'`
      
      console.log(query)
      
      connection.query(query, (err, rows) => {
        if(err) {
          console.log(err)
          resolve(null)
        } else {
          resolve(rows)
        }
      })
    })
  }
  
  var invite = () => {
    return new Promise((resolve, reject) => {
      var query = `SELECT friend_id, user1_id, name FROM friend, user WHERE user2_id = '${user.id}' AND user._id = user1_id AND accept = false`
      
      console.log(query)
      
      connection.query(query, (err, rows) => {
        if(err) {
          console.log(err)
          resolve(null)
        } else {
          resolve(rows)
        }
      })
    })
  }
  
  message().then(message => {
    data.message = message
    return invite()
  }).then(invite => {
    data.invite = invite
    res.json({notice: data})
  }).catch(err => {
    console.log(err)
    res.json(null)
  })
})

router.route('/process/search').post((req, res) => {
  var keyword = req.body.keyword
  var query = `SELECT * FROM user WHERE _id = '${keyword}'`

  console.log(query)
  
  connection.query(query, (err, rows) => {
    if(err) {
      res.json({err: true})
    } else {
      if(rows.length) {
        res.json({err: false, user: rows[0]})
      } else {
        res.json({err: true})
      }
    }
  })
})

router.route('/process/invite').post((req, res) => {
  var user = req.session.user
  var target = req.body.target
  
  var check = () => {
    return new Promise((resolve, reject) => {
      var checkQuery = `SELECT * FROM friend WHERE user1_id = '${user.id}' AND user2_id = '${target}' OR user1_id = '${target}' AND user2_id = '${user.id}';`
      console.log(checkQuery)
      connection.query(checkQuery, (err, rows) => {
        if(err || rows.length) {
          resolve(false)
        } else {
          resolve(true)
        }
      })
    })
  }
  
  var invite = () => {
    return new Promise((resolve, reject) => {
      var query = `INSERT INTO friend VALUES (sha2('${new Date()}', 256), '${user.id}', '${target}', false)`
      console.log(query)
      connection.query(query, (err, rows) => {
        if(err || rows.affectedRows === 0) {
          resolve(false)
        } else {
          resolve(true)
        }
      })
    })
  }
  
  check().then(result => {
    if(result) {
      return invite()
    } else {
      throw new Error()
    }
  }).then(result => {
    if(result) {
      res.send({success: true})
    } else {
      throw new Error()
    }
  }).catch(err => {
    res.send({success: false})
  })
})

router.route('/process/readMessage').post((req, res) => {
  var user = req.session.user
  var messageId = req.body.id
  var query = `DELETE FROM notice WHERE notice_id = '${messageId}'`
  
  console.log(query)
  
  connection.query(query, (err, rows) => {
    if(err || rows.affectedRows === 0) {
      res.json({success: false})
    } else {
      res.json({success: true})  
    }
  })
})

router.route('/process/friendAccept').post((req, res) => {
  var user = req.session.user
  var friendId = req.body.id
  var accept = req.body.accept
  
  var query = accept ? `UPDATE friend SET accept = true WHERE friend_id = '${friendId}'`:`DELETE FROM friend WHERE friend_id = '${friendId}'`
  
  console.log(query)
  
  connection.query(query, (err, rows) => {
    if(err || rows.affectedRows === 0) {
      res.json({success: false})
    } else {
      res.json({success: true})  
    }
  })
})

router.route('/process/deleteFriend').post((req, res) => {
  var user = req.session.user
  var friend = req.body.user
  
  var id1 = user.id
  var id2 = friend.id
  
  var query = `DELETE FROM friend WHERE (user1_id = '${id1}' AND user2_id = '${id2}') OR (user1_id = '${id2}' AND user2_id = '${id1}')`
  
  console.log(query)
  
  connection.query(query, (err, rows) => {
    if(err || rows.affectedRows === 0) {
      res.json({success: false})
    } else {
      res.json({success: true})  
    }
  })
})

app.use(router)

var server = http.createServer(app)
var io = require('socket.io').listen(server)

var users = {} // Connected users
var chatUsers = {} // Connected users (Chatroom)
io.on('connection', socket => {
  socket.on('login', user => {
    users[socket.id] = user
    console.log(`'${user}' was connected (Socket ID: ${socket.id})`)
    io.emit('connectedUsers', users)
  })
    
  socket.on('joinChat', user => {
    socket.join('chat')
    io.to('chat').emit('chatConnected', user)
  })
  
  socket.on('sendMessage', data => {
    var user = data.user
    var message = data.message
    io.to('chat').emit('chatRecive', {user: user, message: message})
  })
  
  socket.on('leaveChat', user => {
    io.to('chat').emit('chatDisconnected', user)
    socket.leave('chat')
  })

  socket.on('disconnect', () => {
    if(users[socket.id]) {
      console.log(`'${users[socket.id]}' was disconnected`)
      delete users[socket.id]
      io.emit('connectedUsers', users)
    }
  })
})

// Server start (Default port: 8080) 
server.listen(8080, () => {
  console.log('Server is running')
})