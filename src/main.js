import axios from 'axios'

import Vue from 'vue'
import VueSocket from 'vue-socket.io'
import App from './App.vue'

Vue.prototype.$http = axios
Vue.config.devtools = true
Vue.use(VueSocket, '/')

new Vue({
  el: '#app',
	render: h => h(App)
})