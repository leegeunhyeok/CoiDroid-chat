# Components

> ## App.vue
> 로그인, 회원가입, 메인 컴포넌트를 제어
>``` javascript
> data: {
>   user: Object, // 사용자의 아이디, 이름 저장객체
>   view: String, // 로그인, 회원가입, 메인 뷰 제어('LOGIN', 'REGISTER', 'MAIN')
>   login: boolean // 현재 로그인 여부 확인 
> }
>
> created() {
>   // 서버에서 세션 유무 확인
>   // 세션이 존재하면 login = true (메인화면으로 전환)
> }
>
> methods: {
>   onLogin(user) {
>     this.user = user
>     this.login = true
>     this.view = 'MAIN'
>     // 로그인 성공시 호출(또는 세션 확인 후 호출)
>   },
>   changeView(viewName) {
>     this.view = viewName
>     // 인자로 전달된 뷰 문자열 적용
>   }
> }
> ```

<hr>

> ## LoginComponent.vue
>
> 로그인
>``` javascript
>data() {
>   return {
>     email: '', // 이메일 Input
>     password: '', // 비밀번호 Input
>     modalTitle: '', // 모달 제목
>     modalBody: '', // 모달 데이터
>     modal: false // (false: 숨김, true: 보여주기)
>   }
>  },
>  methods: {
>   showModal(title, body) {
>     this.modalTitle = title
>     this.modalBody = body
>     this.modal = true
>     // 인자로 넘어온 제목, 내용을 설정하고 모달을 보여줌
>   },
>   onClickRegister() {
>     this.$emit('onClickRegister', 'REGISTER')
>     // 회원가입 화면으로 전환하기위해 이벤트발생 (App.vue)
>   },
>   onSubmit() {
>     ...
>     this.$emit('onLogin', 유저데이터)
>     // 로그인 시도 (로그인 완료시 이벤트 발생)
>   }
> }
>
>```

<hr>

> ## RegisterComponent.vue
>
> 회원가입
>``` javascript
>data () {
>   return {
>     inputName: '', // 이름입력 Input
>     inputEmail: '', // 이메일입력 Input
>     inputPassword: '', // 비밀번호입력 Input
>     inputRePassword: '', // 비밀번호 재입력 Input
>
>     // 형식 확인 (true: 완료, false: 실패)
>     name: false, 
>     email: false,
>     checking: false,
>     password: false,
>     repassword: false,
>
>     // 문제 확인 (true: 문제 있음, false: 문제 없음)
>     nameError: false,
>     emailError: false,
>     passwordError: false,
>     repasswordError: false,
>
>     inProcess: true, // false: 회원가입 제출 후, true: 회원가입 진행 중
>     success: false, // false: 가입 진행 중, true: 가입 성공
>
>     // 모달 
>     modal: false,
>     modalTitle: '',
>     modalBody: ''
>    }
>  },
>  computed: {
>    percent() {
>       // 이름, 이메일, 비밀번호, 비밀번호 재입력 4가지 모두
>       // 문제가 없을경우 100 반환 (각 항목당 25)
>    }
>  },
>  methods: {
>    showModal(title, body) {
>       // 모달 보여주기 
>    },
>    checkName() {
>       // 이름 형식확인 
>    },
>    checkEmail() {
>       // 서버에서 중복체크 후 결과에 따라 제어 
>    },
>    checkPassword() {
>       // 비밀번호 형식체크 
>    },
>    checkRePassword() {
>       // 비밀번호 일치하는지 확인
>    },
>    onClickBack() {
>       this.$emit('onClickBack', 'LOGIN')
>       // 로그인 페이지로 이동하기위해 이벤트 발생 (App.vue)
>    },
>    onSubmit() {
>       // 서버로 데이터 전송 후 가입 여부 확인 
>    }
>  }
>```

컴포넌트 정보는 추후에 계속 업데이트하도록 하겠습니다